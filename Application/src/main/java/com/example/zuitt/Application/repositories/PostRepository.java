package com.example.zuitt.Application.repositories;


import com.example.zuitt.Application.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/*An interface contains marked @Repository contains methods for database manipulation by extending CrudRepository, PostRepository has inherited its pre-defined methods for creating, retrieving, updating, and deleting*/
@Repository
public interface PostRepository extends CrudRepository<Post, Object> {

}
