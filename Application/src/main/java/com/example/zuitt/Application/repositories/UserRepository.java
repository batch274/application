package com.example.zuitt.Application.repositories;

import com.example.zuitt.Application.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Object> {

}
